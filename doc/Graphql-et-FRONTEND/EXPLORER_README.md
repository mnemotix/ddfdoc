# Documentation pour l'explorateur

 
# Explorateur : Facettes/Agrégations

<div align="center" style='margin-bottom:15px'>
	<img src="./readmeImages/aggs.png" style="max-height:220px;max-width: 190vw;">
</div>

Le calcul des Facettes/Agrégations ce reposent à 100% sur la fonctionnalité **aggs** d'ElasticSearch. Le paramètrage est fait coté API en dur, sauf pour le filtrage (voir plus bas).  

Voici la requête elastic qui est faite : 

    Query  = { 
    query: { "bool": { must } }, // par défaut must vaut []
    track_total_hits: true, // return the real number not limited to 10000
    aggs: {
     "places": {
      "meta": {"name": "Aires", "field": "placesForAggs"},
      "terms": {"size": 1000, "field": "placesForAggs.keyword_case_sentitive"}
     }, 
     "domains": {
      "meta": {"name": "Domaines", "field": "domainForAggs"},
      "terms": {"size": 1000, "field": "domainForAggs.keyword_case_sentitive"}
     },
     "registers": {
      "meta": {"name": "Marques lexicales", "field": "lexicalMarksForAggs"},
      "terms": {"size": 1000, "field": "lexicalMarksForAggs.keyword_case_sentitive"}
     },
     "glossaries": {
      "meta": {"name": "Glossaires", "field": "glossaryForAggs"},
      "terms": {"size": 1000, "field": "glossaryForAggs.keyword_case_sentitive"}
      }
     }
    };

Elle retourne comme résultat (simplifié): 

    {
      // c'est cette valeur qui est affiché pour le compte du nombre de mots
      "hits" : {"total" : { "value" : 42866 }}, 
      
      "aggregations" : {
        "places" : {
          "meta" : { "field" : "placesForAggs", "name" : "Aires" },
          "doc_count_error_upper_bound" : 0,
          "sum_other_doc_count" : 138283,
          "buckets" : [
            {"key" : "Monde francophone", "doc_count" : 40332 },
            {"key" : "Afrique", "doc_count" : 18865 }
          ]
        },
        "domains" : {
          "meta" : {"field" : "domainForAggs","name" : "Domaines"},
          "doc_count_error_upper_bound" : 0,
          "sum_other_doc_count" : 13511,
          "buckets" : [
            {"key" : "sciences", "doc_count" : 1392},
            {"key" : "société","doc_count" : 1312}
          ]
        }
      }
    }

Donc les buckets sont des tableaux d'objects : key + count. le problème des aggs est qu'il n'est pas possible d'avoir {key,id,count} ce qui serait très utile. il y a les `multi-terms` mais les résultats produisent des combinaisons ( l'id ne correspond pas à la clé) d'après mes tests. A retester ??

**Pour appeler cette requête Elastic il faut utiliser dans Apollo :**

- requete

      query Aggregations($mustFilters: String)  {
        lexicalSensesAggs(mustFilters: $mustFilters)
      }

- variables 

      {"mustFilters": "[]"}

  **!! NB : mustFilters est une string !!** donc par la suite il faudrat faire un `JSON.stringify()` au tableau des paramètres.

### On peut filtrer les facettes, par exemple si on désire rajouter les filtres suivants : 

+ Aires: "Belgique"
+ marques lexicales : "familier"
+ marques lexicales : "pejoratif"
+ domaine :alimentaire
+ lexique/glossaire: adjectifs

Il faut construire le JSON suivant : 

    const must = [
      { "bool": {
          "minimum_should_match": 1,
          "should": [
              {"term": {"placesForAggs": "Belgique"}}
          ]
      }},
      { "bool": {
          "minimum_should_match": 1,
          "should": [
              {"term": {"lexicalMarksForAggs": "familier"}},
              {"term": {"lexicalMarksForAggs": "péjoratif"}}
          ]
      }},
      { "bool": {
          "minimum_should_match": 1,
          "should": [
              {"term": {"domainForAggs": "alimentaire"}}
          ]
      }},
      { "bool": {
          "minimum_should_match": 1,
          "should": [
              {"term": {"glossaryForAggs": "Adjectifs"}}
          ]
      }}
    ]

au final comme `mustFilters` est une string on a donc avec `JSON.stringify(must)` :

    {"mustFilters":"[{\"bool\":{\"minimum_should_match\":1,\"should\":[{\"term\":{\"placesForAggs\":\"Belgique\"}},{\"term\":{\"placesForAggs\":\"Europe\"}}]}},{\"bool\":{\"minimum_should_match\":1,\"should\":[{\"term\":{\"lexicalMarksForAggs\":\"péjoratif\"}},{\"term\":{\"lexicalMarksForAggs\":\"familier\"}}]}},{\"bool\":{\"minimum_should_match\":1,\"should\":[{\"term\":{\"glossaryForAggs\":\"Adjectifs\"}}]}}]"}


pour utiliser le resultat de la requete il faut faire 

    JSON.parse(data.lexicalSensesAggs)

<i>Il y a actuellement une limitation coté API qui nous oblige à utiliser/retourner du string au lieu d'object/array pour ce genre de requete.</i>


### Requete ET ou OU

Reprenons l'extrait de l'exemple précédent :

      { "bool": {
          "minimum_should_match": 1,
          "should": [
              {"term": {"lexicalMarksForAggs": "familier"}},
              {"term": {"lexicalMarksForAggs": "péjoratif"}}
          ]
      }}

- Pour faire une requete de type <b>ET</b> il faut que `minimum_should_match` soit égale au nombre de `term` dans le `should` ( soit `should.length()`), soit 2 dans l'exemple si dessus 
- Pour faire une requete de type <b>OU</b> il faut que   `minimum_should_match` soit égale à 1.



# Explorateur : Liste de mots qui match les facettes selectionnées

<div align="center" style='margin-bottom:15px'>
	<img src="./readmeImages/explore-list-terms.png" style="max-height:220px;max-width: 190vw;">
</div>




**Requête Apollo :**

- requete

      query Form_Query_Start_With(
        $first: Int, // nombre d'élèment à retourner pour la pagination
        $after: String, // à partir du ième élement, pour la pagination
        $startWithLetter: String, // "a", "b", "c" ... ou "", filtrer les mots commencant par cette lettre
        $startWithNumber: Boolean, // filtrer les mots commencants par des nombre 
        $startWithNonAlphaNum: Boolean, // filtrer les mots commencants par autre chose que lettre ou nombre
        $mustFilters: String, // identique à celle d'en haut.
        $sortings: [SortingInput]
      ) {
        // récupère le count des mots commencants par cette lettre/chiffre pour la pagination
        count: lexicalSensesForWrittenRepStartWithCount(
          startWithLetter: $startWithLetter
          startWithNumber: $startWithNumber
          startWithNonAlphaNum: $startWithNonAlphaNum
          mustFilters: $mustFilters
        )
        lexicalSenses: lexicalSensesForWrittenRepStartWith(
          startWithLetter: $startWithLetter
          startWithNumber: $startWithNumber
          startWithNonAlphaNum: $startWithNonAlphaNum
          first: $first
          after: $after
          mustFilters: $mustFilters
          sortings: $sortings
        )
      }

- variables 

      {
        "first":54,"after":null,
        "startWithLetter":"c","startWithNumber":false,"startWithNonAlphaNum":false,
        "mustFilters":"[]","sortings":[{"sortBy":"canonicalFormWrittenRep","isSortDescending":false}]
      }

***Donc pour filtrer sur les mots qui commencent par un numérique***

      {
        "startWithLetter": "", "startWithNumber": true, "startWithNonAlphaNum":false,  ...
      }


***Donc pour filtrer sur les mots qui commencent par autre choses que alpha-numérique ( ! - @ & etc)***

      {
        "startWithLetter": "", "startWithNumber": false, "startWithNonAlphaNum":true, ...
      }
