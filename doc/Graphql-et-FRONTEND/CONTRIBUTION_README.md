# Documentation pour la contribution


On peut désormais rajouter des liens entre une définition et 
- des catégories grammaticales ( singulier,pluriel,masculin,féminin, adjectif,pronom etc)
- des exemples
- lieux géographiques
- des marques d'usages : 
   - domaine
   - temporalité
   - registre
   - fréquence
   - connotation
   - genre textuel
   - argot
   - contrainte grammaticale
- une autre définition ( de la même forme ou non)
- lexique

La plupart utilisent des données issus de vocabulaires controlés. 

------------
## Récupération des données pour un vocabulaire spécifique

🔴 IMPORTANT
<span style="color:'red'">**Tout les ajouts suivants utilisent des id provenant de vocabulaires controlés.**</span>
L'idéal est de faire un composant générique qui requete un vocabulaire donné, affiche les résultats et permet la séléction d'un élement. L'id du vocabulaire sera indiqué ci dessous pour chaque type (dans `scheme`). 

<div align="center" style='margin-bottom:15px'>
	<img src="./readmeImages/contrib-domaine.png" style="max-height:220px;max-width: 190vw;">
</div>


- requete apollo

      query GrammaticalProperties($startWith: String, $first: Int, $scheme: String!, $scopeNote: String, $isTopInScheme: Boolean) {
        grammaticalPropertiesStartWith(
          startWith: $startWith // pour filtrer les prefLabel contenant cette chaine, 
          scheme: $scheme
          first: $first
          isTopInScheme: $isTopInScheme 
          scopeNote: $scopeNote
          sortings: [{sortBy: "prefLabel"}]
        )
      }

- variables

      {"first":14,
      "scheme":"http://data.dictionnairedesfrancophones.org/authority/domain",
      "isTopInScheme":true}

- resultat 

      {
        "data": {
          "grammaticalPropertiesStartWith": "{\"edges\":[{\"node\":{\"id\":\"http://data.dictionnairedesfrancophones.org/authority/domaine/handisport\",\"prefLabel\":\"handisport\",\"definition\":\"sport pratiqué par les handicapés physiques\"}}]}"
        }
      }

**NB**
- il faut faire un `JSON.parse(data.grammaticalPropertiesStartWith)`

- isTopInScheme : boolean. Par défaut, donc quand l'utilisateur n'a rien saisi pour filtrer, on affiche les schéma de haut niveau, en gros les plus fréquent ou utile. Si l'utilisateur saisi qlq chose il faut enlever ce filtrage en plus d'envoyer la string saisie:

      {"first":14,"startWith":"informat","scheme":"http://data.dictionnairedesfrancophones.org/authority/domain"}

- scopeNote : `"distinctWrittenRep"` ou `"sameWrittenRep"`, pour les liens entre définitions, il faut filtrer les données selon le type de relation que l'on est en train d'ajouter. 
`"distinctWrittenRep"` ce type/object ne doit etre utilisé que pour des relations entre une définition et une définition d’une autre forme.
`"sameWrittenRep"` ce type/object ne doit etre utilisé que pour des relations entre une 2 définitions de la meme forme.


----------

## Ajout d'une catégorie grammaticale ( singulier,pluriel,masculin,féminin, adjectif,pronom etc)

Cetté étape ce fait lors de la création de la form ou de son édition. 

Elle suit un arbre décisionnel qui doit refleter ce diagrame :

<div align="center" style='margin-bottom:15px'>
	<img src="./readmeImages/cat-gram-schema.png" style="max-width: 90vw;">
</div>

On utilise plusieurs vocabulaires controlés tout au long de cette étape.

1) Pour la première étape on utilise celui de `part-of-speech-type`:
<div align="center" style='margin-bottom:15px'>
	<img src="./readmeImages/cat-gram-1.png" style="max-height:320px">
</div>

2A) Si l'utilisateur choisi `verbe` à l'étape 1, on affiche le vocabulaire de `verb-frame`  puis celui de `tense`, `mood` et `person`

<div   style='display:flex;'>
  <div align="center" style='margin:15px;'>
    <img src="./readmeImages/transitivity.png" style="max-height:240px">
  </div> 
  <div align="center" style='margin:15px'>
    <img src="./readmeImages/tense-mood-person.png" style="max-height:300px">
  </div>
</div>

- requete Apollo (générique pour toute la partie contribution) pour sauvegarder cette mutation:

      mutation UpdateLexicalSense(
        $lexicalSenseId: ID!,  
        $grammaticalCategoryId: String,
        $definition: String, $writtenRep: String, $usageExample: UsageExampleInput, 
        $transitivityId: String, $moodId: String, $tenseId: String,
        $personId: String, $genderId: String , $numberId: String,
        $placeId: String, $registerId:String, $connotationId:String
      ) {
        updateLexicalSense(
          input: {
            objectId: $lexicalSenseId, grammaticalCategoryId: $grammaticalCategoryId, definition: $definition,writtenRep: $writtenRep, usageExample: $usageExample,transitivityId: $transitivityId, moodId: $moodId, tenseId: $tenseId, personId: $personId, genderId: $genderId, numberId: $numberId, placeId: $placeId, registerId: $registerId, connotationId: $connotationId
          }
        ) { updatedObject {id}}
      }

- variables :

      {
        writtenRep: 'pate',
        lexicalSenseId: 'lexical-sense/rbdd57w9csfcou',
        grammaticalCategoryId: 'lexinfo:verb',
        transitivityId: 'ddfa:verb-frame/transitiveNominalComplementFrame',
        tenseId: 'ddfa:tense/futureTense',
        moodId: 'ddfa:mood/conditionalMood',
        personId: 'ddfa:person/secondPersonSingular'
      }

 
2B) Si en étape 1 l'utilisateur choisi parmis `adverbe, conjunction, interjection ou  particule` on n'affichera plus d'étape suivante. On sauvegarde directement.


- variables apollo :

      {
        writtenRep: 'pate2',
        lexicalSenseId: 'lexical-sense/rbdd57w9csfcou',
        grammaticalCategoryId: 'lexinfo:adverb',
        transitivityId: 'ddfa:verb-frame/transitiveNominalComplementFrame' // transitif
      }

2C) Si en étape 1 l'utilisateur choisi autre chose que `verbe, adverbe, conjunction, interjection ou particule` on affiche le vocabulaire de `gender` et `number`:
<div align="center" style='margin-bottom:15px'>
	<img src="./readmeImages/gender-number.png" style="max-height:320px">
</div>

- variables apollo :

      {
        "writtenRep":"pate","lexicalSenseId":"lexical-sense/rbdd57w9csfcou",
        "grammaticalCategoryId":"lexinfo:properNoun",
        "transitivityId":"ddfa:verb-frame/unspecifiedFrame",
        "genderId":"ddfa:gender/unstableGender",
        "numberId":"ddfa:number/singulare-tantum"
      }


**NB: coté API quand l'utilisateur change la catégorie grammaticale d'un mot une nouvelle `LexicalEntry` est crée et l'ancienne supprimée. Cela permet d'éviter de devoir supprimer tel ou tel attribut (par exemple le genre et nombre si on choisi une catégorie qui n'en a pas)**

---------------

Voici le code JS qui permet de gérer l'arbre de décision : 

     /**
      * the keys of this object are the current step.
      * each key value return the next step according to other parameters ( often choosed by user )
      * _SUBMIT is the end of the tree
      */
      export const decisionTree = {
        _FORM: () => _DEFINITION, // on commence par l'input avec la form, après il faudra saisir la définition dans un textArea
        _DEFINITION: ({showStepPlace}) => { // après la déf il faut soit saisir la Place soit la catégorie ( on saute la place en mode édition en faite )
          if (showStepPlace) {
            return _PLACE;
          } else {
            return _CATEGORY;
          }
        },
        _PLACE: () => _CATEGORY,
        _CATEGORY: ({lexicalEntryTypeName, formikValues}) => {
          /*
           On a une liste exhaustive des valeurs possible ici pour éviter les erreures.
          
           word = mot sans espace
           multiwordexpression = suite de mot avec des espaces donc
           affix = qui commence ou fini par un tiret
          */ 
          switch (lexicalEntryTypeName.toLowerCase()) {
            case "word":
              switch (formikValues?.grammaticalCategory?.id?.toLowerCase()) {
                case "lexinfo:verb": // si l'utilisateur choisi "verbe" on affichera le voc de "gender" + "nomber"
                  return _TRANSITIVITY;
                case "lexinfo:adverb":
                case "lexinfo:conjunction":
                case "lexinfo:interjection":
                case "lexinfo:particle":
                  return _SUBMIT;
                case "lexinfo:possessiveAdjective": case "lexinfo:definiteArticle":
                case "lexinfo:partitiveArticle": case "lexinfo:demonstrativePronoun":
                case "lexinfo:indefinitePronoun": case "lexinfo:interrogativePronoun":
                case "lexinfo:personalPronoun": case "lexinfo:possessivePronoun":
                case "lexinfo:relativePronoun": case "lexinfo:adjective":
                case "lexinfo:article": case "lexinfo:noun": case "lexinfo:properNoun":  
                case "lexinfo:preposition": case "lexinfo:pronoun": case "lexinfo:verb":
                default:
                  return _GENDERNUMBER;
              }
            case "multiwordexpression":
            case "affix":
            default:
              return _SUBMIT;
          }
        },

        _TRANSITIVITY: ({formikValues}) => {
          if (formikValues?.infinitif?.id === true) {
            return _SUBMIT;
          } else {
            return _TENSEMOODPERSON;
          }
        },
        _GENDERNUMBER: () => _SUBMIT,
        _TENSEMOODPERSON: () => _SUBMIT,
        _USAGEEXAMPLE: () => _SUBMIT
      };



-----------



## Ajout d'un exemple

nom de l'attribut utilisé : `usageExample` qui est un object composé de 2 chaines de characttère de taille libre (type textArea): "value" et  "bibliographicalCitation" 
 

- requete

      mutation UpdateLexicalSense_usages(
        $lexicalSenseId: ID!,
        $writtenRep: String!,
        $usageExample: UsageExampleInput, 
      ) {
        updateLexicalSense(input: {
          objectId: $lexicalSenseId,       
          writtenRep: $writtenRep,
          usageExample: $usageExample 
        }) { updatedObject { id } }
      }


- variables

      {
        "lexicalSenseId":"lexical-sense/3vfekfmaa64a4h",
        "writtenRep":"pate",
        "usageExample":{
          "value":"l'exemple ",
          "bibliographicalCitation":"la source"
        }
      }

## Edition d'un exemple 

même requete que pour l'ajout, on rajoute l'attribut "id" pour indiquer à l'API quel exemple il faut éditer :

- variables 

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        usageExample: {
          id: 'usage-example/0s6i5a5n0vhpj5',
          value: 'l\'exemple ',
          bibliographicalCitation: 'la sourcett'
        }
      }

## Ajout d'un lieu géographique 

`placeId`

- requete

      mutation UpdateLexicalSense_places($lexicalSenseId: ID!, $writtenRep: String!, $placeId: String!) {
        updateLexicalSense(
          input: {objectId: $lexicalSenseId, writtenRep: $writtenRep, 
        placeId: $placeId}) 
        { updatedObject { id } }
      }

- variables

      {"lexicalSenseId":"lexical-sense/3vfekfmaa64a4h","writtenRep":"pate","placeId":"geonames:2992166"}

## Suppression d'un lieu géographique 

`placeIdsToDelete`

- requete

      mutation DeleteLexicalSense_places( $lexicalSenseId: ID!,  $writtenRep: String!, $placeId: String! ) {
        updateLexicalSense(
          input: {
            objectId: $lexicalSenseId,
            writtenRep: $writtenRep,
            placeIdsToDelete : [$placeId]
          }
        ) { updatedObject { id } }
      }
 
- variables

      {"lexicalSenseId":"lexical-sense/3vfekfmaa64a4h","writtenRep":"pate",
      "placeId":"geonames:6612877"}



## Ajout d'un domaine


- scheme du voc controlé : `domaine`
- requete


      mutation UpdateLexicalSense_domain(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $domainId: String!,             <----------
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            domainId: $domainId
          }) { updatedObject { id }}
        }


- variables

      {"lexicalSenseId":"lexical-sense/3vfekfmaa64a4h","writtenRep":"pate",
      "domainId":"http://data.dictionnairedesfrancophones.org/authority/domaine/sports-d-hiver"}

## Suppression d'un domaine 

`domainIdToDelete`
- requete


      mutation DeleteLexicalSense_domain( $lexicalSenseId: ID!,  $writtenRep: String!, $domainId: String! ) {
        updateLexicalSense(
          input: {
            objectId: $lexicalSenseId,
            writtenRep: $writtenRep,
            domainIdToDelete : $domainId
          }
        ) {
          updatedObject {
            id      
          }
        }
      }
 - variables 

    {
      lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
      writtenRep: 'pate',
      domainId: 'ddfa:domaine/sports-d-hiver'
    }

## Ajout d'une temporalité

 
- scheme du voc controlé : `temporality`
- requete


      mutation UpdateLexicalSense_temporality(
        $lexicalSenseId: ID!,
        $writtenRep: String!,
        $temporalityId: String!,              <----------
        ) {
        updateLexicalSense(input: {
          objectId: $lexicalSenseId,       
          writtenRep: $writtenRep,
          temporalityId: $temporalityId
        })  { updatedObject { id }}
      }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        temporalityId: 'http://data.dictionnairedesfrancophones.org/authority/temporality/newUsage'
      }


## Suppression d'une temporalité

`temporalityIdToDelete`
- requete

      mutation DeleteLexicalSense_temporality( $lexicalSenseId: ID!,  $writtenRep: String!, $temporalityId: String! ) {
        updateLexicalSense(
          input: {
            objectId: $lexicalSenseId,
            writtenRep: $writtenRep,
            temporalityIdToDelete : $temporalityId             <----------
          }
        )  { updatedObject { id }}
      }

- variables
 
      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        temporalityId: 'ddfa:temporality/outdatedUsage'
      }
 

## Ajout d'un registre

- scheme du voc controlé : `register`
- requete


      mutation UpdateLexicalSense_register(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $registerId: String!,              <----------
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            registerId: $registerId
          }) { updatedObject { id } }
        }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        registerId: 'http://data.dictionnairedesfrancophones.org/authority/register/traditionalRegister'
      }


## Suppression d'un registre

`registerIdToDelete`

## Ajout d'une fréquence

- scheme du voc controlé : `frequency`
- requete


      mutation UpdateLexicalSense_frequency(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $frequencyId: String!,              <----------
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            frequencyId: $frequencyId
          }) { updatedObject { id } }
        }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        frequencyId: 'http://www.lexinfo.net/ontology/2.0/lexinfo#rarelyUsed'
      }

## Ajout d'une connotation

- scheme du voc controlé : `connotation`
- requete

      mutation UpdateLexicalSense_connotation(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $connotationId: String!,              <----------
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            connotationId: $connotationId
          }) { updatedObject { id } }
        }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        connotationId: 'http://data.dictionnairedesfrancophones.org/authority/connotation/mokingConnotation'
      }

## Suppression d'une connotation

`connotationIdToDelete`

## Ajout d'un genre textuel

- scheme du voc controlé : `textual-genre`
- requete

      mutation UpdateLexicalSense_TextualGenre(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $textualGenreId: String!,              <----------
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            textualGenreId: $textualGenreId
          }) { updatedObject { id } }
        }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        textualGenreId: 'http://data.dictionnairedesfrancophones.org/authority/register/didacticGenre'
      }

## Suppression d'un genre textuel 

`textualGenreIdToDelete`

## Ajout d'un argot (sociolect)

- scheme du voc controlé : `sociolect`
- requete

      mutation UpdateLexicalSense_sociolect(
        $lexicalSenseId: ID!, 
        $writtenRep: String!,
        $sociolectId: String!             <----------
      ) {
        updateLexicalSense(input: {objectId: $lexicalSenseId, writtenRep: $writtenRep, sociolectId: $sociolectId}) 
        { updatedObject { id } }
      }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        sociolectId: 'http://data.dictionnairedesfrancophones.org/authority/sociolect/unspecifiedSociolect'
      }


## Ajout d'une contrainte grammaticale

- scheme du voc controlé : `grammatical-constraint`
- requete

      mutation UpdateLexicalSense_grammaticalConstraint(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $grammaticalConstraintId: String!,              <----------
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            grammaticalConstraintId: $grammaticalConstraintId 
          }) { updatedObject { id } }
        }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        grammaticalConstraintId: 'http://data.dictionnairedesfrancophones.org/authority/grammatical-constraint/epithete'
      }
## Suppression d'une contrainte grammaticale

`grammaticalConstraintIdToDelete`

## Ajout d'un lexique

- scheme du voc controlé : `glossary`
- requete


      mutation UpdateLexicalSense_glossary(
        $lexicalSenseId: ID!,
        $writtenRep: String!,
        $glossaryId: String!,              <----------
      ) {
        updateLexicalSense(input: {
          objectId: $lexicalSenseId,       
          writtenRep: $writtenRep,
          glossaryId: $glossaryId      
        }) { updatedObject { id } }
      }

- variables

      {
        lexicalSenseId: 'lexical-sense/3vfekfmaa64a4h',
        writtenRep: 'pate',
        glossaryId: 'http://data.dictionnairedesfrancophones.org/authority/glossary/fromages-forts'
      }

## Suppression d'un lexique

`glossaryIdToDelete`

-----------
# Ajout d'un lien vers une autre définition


On peut rajouter un lien vers une définition de la même forme ou d'une autre forme. 
Le vocabulaire controlé utilisé est `sense-relation` mais il faut filtrer les entrées selon l'attribut `scopeNote` car certaines `sense-relation` ne doivent être utiliser que pour des relations entre :
- 2 définitions de la meme forme  (  `scopeNote == sameWrittenRep`)
- une définition et une définition d’une autre forme (  `scopeNote == distinctWrittenRep`)
 
Il faut utiliser l'attribut `scopeNote` de `grammaticalPropertiesStartWith` bon récupérer les liens possibles adéquats.




## Ajout d'un lien vers 2 définitions de la même forme 


- scheme du voc controlé `sense-relation`
- requete

      mutation UpdateLexicalSense_semanticRelation(
          $lexicalSenseId: ID!,
          $writtenRep: String!,
          $semanticRelationInput: SemanticRelationInput,    <-----------
          $includeCreator: Boolean = true
        ) {
          updateLexicalSense(input: {
            objectId: $lexicalSenseId,       
            writtenRep: $writtenRep,
            semanticRelationInput: $semanticRelationInput    
          }) { updatedObject { id } }
        }

- variables

      {
        "includeCreator": true,
        "lexicalSenseId": "lexical-sense/3vfekfmaa64a4h",
        "writtenRep": "pate",
        "semanticRelationInput": { // il faut passer semanticRelationInput
          "semanticRelationType": {  // avec l'id choisi parmis les voc controllés
            "id": "http://data.dictionnairedesfrancophones.org/authority/sense-relation/approximateSynonym" 
          },
          "semanticRelationOfLexicalSenseInput": { 
             // et l'id de la définition que l'utilisateur a choisi
            "id": "dict:bdlp/sense/bebf3066feadda21ef3fed1510496e4" 
           }
        }
      }


## Ajout d'un lien vers une autre définition d'une autre forme

C'est exactement la même requete que celle entre 2 définitions de la même forme. 

    
  
 

 

 

shit + cmd + v 