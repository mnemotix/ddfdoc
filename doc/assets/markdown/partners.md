Partenaires
======================

Direction du projet, maîtrise d'ouvrage et expertise scientifique :

<img src="../images/partners/lyon3.png" alt="Lyon 3" style="max-width:240px">
![2IF](../images/partners/iif.jpg)


Avec le soutien de :

![Culture](../images/partners/ministre-culture.jpg)
![AUF](../images/partners/auf.jpg)
![Wikimédia France](../images/partners/wikimedia.png)
![OIF](../images/partners/oif.jpg)
<br>
![CNRS](../images/partners/cnrs.jpg)
![Huma-Num](../images/partners/humanum.jpg)


En partenariat avec :

![TV5Monde](../images/partners/tv5monde.jpg)
![Medias](../images/partners/logo_france_medias_monde.png)
![Institut Français](../images/partners/logo-institut-francais.jpg)
![Fondation Alliance Française](../images/partners/faf.jpg)


Ils contribuent au contenu :

![Wiktionnaire](../images/partners/wiktionary.jpg)
![UCLouvain](../images/partners/louvain_saint-louis.jpg)
![ULaval](../images/partners/laval.png)
![OQLF](../images/partners/oqlf.jpg)
![ATILF](../images/partners/atilf.jpg)
![ASOM](../images/partners/asom.jpg)
