# Compte utilisateur

Les comptes personnels permettent de participer à l’enrichissement du Dictionnaire des francophones, mais aussi de personnaliser son affichage en enregistrant une préférence géographique, mais aussi thématique. Peu d’informations sont nécessaires, afin de ne pas collecter abusivement des données personnelles, et celles-ci servent surtout à mieux connaître les personnes qui participent au site.

<br>

# Recherche simple <a id="recherche-niv1"></a>

Pour rechercher un mot ou une expression, il suffit de les saisir dans la barre en haut de l’écran et de valider. Des suggestions apparaissent à mesure de la saisie afin d’aider à la découverte des termes recherchés.

Dans le cas où le terme entré est connu, une liste de définitions qui correspondent exactement au mot recherché s’affiche au centre de l’écran. Les accents sont importants, et le Dictionnaire des francophones différencie les majuscules et les minuscules.

Lorsque la suite de lettres saisie ne dispose pas d’une entrée, une liste de mots proches apparaît afin de suggérer des termes proches qui pourraient apporter des informations liées à la recherche.

L’ordre d’affichage des définitions dépend ensuite de l’information géographique saisie sur la page d’accueil, ou sélectionnée dans un profil utilisateur. Les couleurs correspondent aux grandes zones du monde.

<br>

# Explorer le contenu <a id="recherche-niv2"></a>

Depuis le 16 mars 2022, la version 3.0 offre un nouvel outil d’exploration, Explorer, qui permet de filtrer le contenu selon quatre paramètres combinables : les aires géographiques (Québec, Côte d’Ivoire, etc.), les glossaires (arbres, virus, etc.), les marques lexicales (familier, vieilli, etc.) et les domaines (géographie, musique, etc.). Cet outil est un moteur de recherche à facettes qui propose une option pour que les critères soient cumulatifs ou exclusifs, afin d’affiner les recherches.

<br>

# Recherche experte <a id="recherche-niv3"></a>

Il est également possible de faire des requêtes directes dans la base de connaissance par le biais d’un [point d’accès SPARQL](https://www.dictionnairedesfrancophones.org/sparql). Quelques exemples de requêtes sont documentées dans [le dépôt Gitlab du DDF](https://gitlab.com/mnemotix/ddfdoc/-/tree/master/doc/sparql).
