Nous rejoindre
======================

## Aidez nous à créer le dictionnaire qui décrit le mieux tous les usages du français parlé par les francophones du monde entier ! 

Le *Dictionnaire des francophones* est un outil participatif. Il intègre des ressources produites par un grand nombre de personnes, au fil du temps, et il est augmenté des apports faits par toute personne qui souhaite participer ! Les Francophones vivent dans des régions très variées du monde, avec des spécificités propres à leurs usages, et l’aide de chacun et chacune sera nécessaire pour réussir à décrire tous les détails de la langue française.

## Pourquoi contribuer au *Dictionnaire des francophones* ?

* Améliorer les connaissances sur la langue française.
* Transmettre des connaissances spécifiques ou rares.
* Développer la pluralité des visions du monde représentées.

## Comment contribuer ?

Grâce à un compte utilisateur, il est possible d’ajouter de nouvelles définitions, pour des mots déjà décrits ou pour des mots pas encore décrits, mais aussi des exemples d’usage, des indications sur l’usage des mots, des indications de liens entre les mots. L’ajout d’informations sur une aire d’usage (je le dis comme ça chez moi) ou sur une indication de glossaire (c’est un mot de la botanique) est plus aisé que la rédaction d’une nouvelle définition. Chaque nouvelle contribution est une touche supplémentaire qui améliore progressivement le *Dictionnaire des francophones*, et chaque information compte !

Il est aussi possible de valider des informations déjà présentes ou de signaler les problèmes éventuels, qu’ils soient de mise en forme ou du contenu. C’est un site participatif, dont la qualité va dépendre des informations, mais aussi des relectures successives qui assureront de la bonne qualité du contenu ajouté.

Faire savoir que le *Dictionnaire des francophones* existe permet d’accroitre son lectorat et son contributorat. Parlez-en autour de vous ! 
