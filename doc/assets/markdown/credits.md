Crédits
======================

# Pilotage
<div align="center" style='margin-bottom:15px'>
	<img src="../images/partners/Lyon3-2IF.png" alt="opérateur du projet" style="max-height:120px;max-width: 90vw;">
</div>

La propriété du projet et la direction opérationnelle et éditoriale du *Dictionnaire des francophones* est assurée par l’Institut international pour la Francophonie – Université Jean Moulin Lyon 3.

Le *Dictionnaire des francophones* est piloté par un consortium d’acteurs français et francophones issus du monde universitaire et de la recherche ainsi que de représentants de l’administration.

Les orientations techniques et scientifiques du *Dictionnaire des francophones* sont déterminées par son Conseil Scientifique qui est composé de : Bernard Cerquiglini, Paul de Sinety, Mathieu Avanzi, Olivier Baude, Claudine Bavoux, Aïcha Bouhjar, Barbara Cassin, Moussa Daff, Kaja Dolar, Ginette Galarneau, Pierre Geny, François Grin, Katia Haddad, Latifa Kadi-Ksouri, Jean-Marie Klinkenberg, Jérémie Kouadio, Mona Laroussi, Gabrielle Le Tallec Lloret, Lucas Lévêque, Franck Neveu, Marie Steffens, Jean Tabi Manga.

Le *Dictionnaire des francophones* est ou a été soutenu financièrement par le ministère de la Culture français, l’Université Jean Moulin Lyon 3, l’Agence universitaire de la Francophonie, l’association Wikimédia France, l’Organisation internationale de la Francophonie et la très grande infrastructure de recherche Huma-Num-CNRS.

# Développement

<div align="center" style='margin-bottom:15px;margin-top:15px'>
<img src="../images/partners/mnemotix.png" alt="Mnémotix" style="max-width:300px;margin:auto">
</div>

Le développement de la base de connaissance et du site internet du *Dictionnaire des francophones* a été réalisé par la société d’intérêt collectif [Mnémotix](https://www.mnemotix.com/), suite à la publication d’un marché public. 

# Réalisation 

* Directeur de la publication : Éric Carpano, président de l’Université Jean Moulin Lyon 3
* Chef de projet : Noé Gasparini
* Responsable des données : Sébastien Gathier
* Responsable des communautés : Nadia Sefiane
* Équipe de conception : Noé Gasparini, Kaja Dolar, Marie Steffens
* Conception de l’ontologie : Jean Delahousse, Noé Gasparini, Freddy Limpens
* Création du logo et de la charte graphique : [Alexandra Simon](https://alexandrasimon.fr/)
* Design UX initial : [Jérémie Cohen](https://jeremiecohen.fr/)
* Numérisation de trois ressources : Laurent Catach ([4H Conseil](https://www.4h-conseil.fr/)) 
* Équipe de développement : [Mnémotix](https://www.mnemotix.com/)  (Freddy Limpens, Nicolas Delaforge, Mylène Leitzelman, Pierre-René Lherisson, Mathieu Rogelja, Florian Cuny, Application web v1 : Quentin Richaud, Application web v2 & v3 : Alain Ibrahim) 
* Équipe de développement des applications mobiles : [Brave Cactus](https://brave-cactus.com/fr/) (Alison Andrault, Olivier Bernal, Benjamin Hamon, Cindy Hamon, Sophie Huart)
* Conception des interfaces : [Makewaves](https://makewaves.fr/) et [Atelier Jugeote](https://atelier-jugeote.com/) (Antoine Roquis, Manon Verbeke, Melvin Buseniers, Raphaelle Penhoud, Anaëlle Razafimamonjiarison) 
* Équipe à l’[Institut international pour la Francophonie-Lyon 3](https://2if.universite-lyon.fr/) : Manuel Jobert, Guillaume Godineau, Noé Gasparini, Sébastien Gathier, Nadia Sefiane, Aurore Sudre, Hong Khanh Dang, Romain Bethoule, Hugo Casamian (et pendant un temps : Olivier Garro, Marielle Payaud, Sandrine David, Camelia Danc, Thomas Meszaros, Léa Bugin, Antoine Bouchez, Lucas Prégaldiny, Louisa Curci, Florine Chatillon, Lili Bentzinger)
* Hébergement : Huma-Num, infrastructure CNRS
