#Nombre d’entrées et de définitions dans une ressource du Dictionnaire des francophones sans les flexions (ici, dans la partie collaborative du DDF)).

``` sparql
#Nombre d’entrées et de définitions dans une ressource du Dictionnaire des francophones sans les flexions (ici, dans la partie collaborative du DDF)).
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT (COUNT(DISTINCT ?lentry) AS ?nombre_de_mots) (COUNT(DISTINCT ?definition) AS ?nombre_de_definitions)
WHERE {
 ?entry a lexicog:Entry .
 ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/ddf> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/inventaire> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/wiktionnaire> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/bdlp> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/gdt> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/belgicismes> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/drf> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/asom> .
# ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/franceterme> .
 ?entry lexicog:describes ?lentry .
 ?lentry ontolex:sense ?definition .
     MINUS {?lentry a ddf:VerbalInflection.}
     MINUS {?lentry a ddf:Inflection.}
}
```
