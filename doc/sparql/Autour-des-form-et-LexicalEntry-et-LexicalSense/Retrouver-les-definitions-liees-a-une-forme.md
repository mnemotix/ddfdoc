# Retrouver les définitions liées à une forme

Soit: 
* une forme donnée :  `values ?writtenRep {"ballotage"@fr } .`
* ou qui commence par "ballot", 2 manières de filtrer:
  * `FILTER regex(?writtenRep, "ballot", "i") .`
  * `FILTER STRSTARTS(?writtenRep, "ballot") .`

Ces différents cas sont écrits dans la requête suivante; a tester en ajoutant/retirant le marqueur de commentaire de ligne `#`

```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <http://www.geonames.org/ontology#>
PREFIX dict: <http://data.dictionnairedesfrancophones.org/dict/>

SELECT  distinct *
WHERE {    
   # values ?writtenRep {"ballotage"@fr } .
    
   ?lexicalEntry ontolex:canonicalForm ?form .
   ?form ontolex:writtenRep ?writtenRep .
   ?lexicalEntry ontolex:sense ?lexicalSense . 
   ?lexicalSense skos:definition ?definition . 
 #  FILTER regex(?writtenRep, "ballot", "i") .
    FILTER STRSTARTS(?writtenRep, "ballot") .
}
 
LIMIT 200
```
