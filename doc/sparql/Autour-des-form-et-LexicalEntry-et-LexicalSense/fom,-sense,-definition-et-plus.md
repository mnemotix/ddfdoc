# Récupération des éléments pour une forme donnée


```sparql

PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <http://www.w3.org/ns/prov-o-20130430#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX senserel:  <http://data.dictionnairedesfrancophones.org/authority/sense-relation/> 
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
PREFIX onto: <http://www.ontotext.com/>

 
 

SELECT distinct ?lexicalEntry ?writtenRep ?def ?loc  ?pos  ?multi  ?gender ?number  ?number ?transitivity ?term  
?register ?domain 
WHERE {
    ?lexicalEntry ontolex:canonicalForm/ontolex:writtenRep ?writtenRep .
    FILTER( ?writtenRep = "pomme"@fr ).
    ?lexicalEntry rdf:type ?type .
    FILTER( ?type = ontolex:LexicalEntry ) . 

    ?lexicalEntry ontolex:sense ?lexicalSense .
    ?lexicalSense skos:definition ?def .

    OPTIONAL{  ?lexicalSense ddf:hasLocalisation/skos:prefLabel ?loc }

    optional{ ?lexicalEntry ddf:hasPartOfSpeech/skos:prefLabel ?pos }.
    optional{ ?lexicalEntry ddf:multiWordType/skos:prefLabel ?multi } .
    optional{ {?lexicalEntry ddf:hasGender/skos:prefLabel ?gender} UNION
    {?lexicalEntry ddf:inflectionHasGender/skos:prefLabel ?gender}}.
    optional{ {?lexicalEntry ddf:hasNumber/skos:prefLabel ?number } UNION
    {?lexicalEntry ddf:inflectionHasNumber/skos:prefLabel ?number }} .
    optional{ ?lexicalEntry ddf:hasTransitivity/skos:prefLabel ?transitivity } .
    optional{ ?lexicalEntry lexinfo:termElement/skos:prefLabel ?term }.
    optional{ ?lexicalSense ddf:hasDomain/skos:prefLabel ?domain } .
    optional{ ?lexicalSense ddf:hasRegister/skos:prefLabel ?register } .

    # optional { 
    #    # on récupère les localisations de sense, groupé par sense pour éviter d'avoir 50 lignes en sortie pour le meme sense
    #    # le AS permet de stocker le résultat du concat dans ?senseLocalisation
    #    SELECT ?sense (GROUP_CONCAT(?loc; SEPARATOR=", ") AS ?senseLocalisation)
    #    # ddf:hasLocalisation/skos:prefLabel => "property paths"  on récupère la propriété 'ddf:hasLocalisation'`' puis sa sous propriété  'skos:prefLabel' 
    #    WHERE{ ?sense ddf:hasLocalisation/skos:prefLabel ?loc } GROUP BY ?sense
    # }
}

LIMIT 100
```