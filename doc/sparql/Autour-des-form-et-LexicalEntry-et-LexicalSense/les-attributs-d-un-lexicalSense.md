# Tous les attributs d'un LexicalSence donnée

Cette requête liste tout les attributs [NDR:que j'ai trouvé] sur un lexicalSense donné (<http://data.dictionnairedesfrancophones.org/dict/wikt/sense/140ae3662fce0310dda46cb4e3c013f2>)

**NB** : Équivalent à la requete /form/eau/sense/wkt%3Asense%2F140ae3662fce0310dda46cb4e3c013f2
  
```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <http://www.geonames.org/ontology#>

SELECT  distinct *
WHERE {    
   ?lexicalEntry ontolex:canonicalForm ?form .
   ?form ontolex:writtenRep ?writtenRep .
   ?lexicalEntry ontolex:sense ?lexicalSense . 
   ?lexicalSense skos:definition ?definition .
   FILTER( ?writtenRep = "eau"@fr )
   FILTER (?lexicalSense = <http://data.dictionnairedesfrancophones.org/dict/wikt/sense/140ae3662fce0310dda46cb4e3c013f2> ) 
   optional{ ?lexicalEntry ddf:hasPartOfSpeech/skos:prefLabel ?pos }.
   optional{ ?lexicalEntry ddf:multiWordType/skos:prefLabel ?multi } .
   optional{ { ?lexicalEntry ddf:hasGender/skos:prefLabel ?gender} UNION
           { ?lexicalEntry ddf:inflectionHasGender/skos:prefLabel ?gender}}.
   optional{ { ?lexicalEntry ddf:hasNumber/skos:prefLabel ?number } UNION
             { ?lexicalEntry ddf:inflectionHasNumber/skos:prefLabel ?number }} .
   optional{ ?lexicalEntry ddf:hasTransitivity/skos:prefLabel ?transitivity } .
   optional{ ?lexicalEntry lexinfo:termElement/skos:prefLabel ?term }.
   optional{ ?lexicalSense ddf:hasDomain/skos:prefLabel ?domain } .
   optional{ ?lexicalSense ddf:hasRegister/skos:prefLabel ?register } .
   optional{ ?lexicalSense ddf:hasSemanticProperty/skos:prefLabel ?semanticProperty }.
   optional{ ?lexicalSense ddf:hasTemporality/skos:prefLabel ?temporality }.
   optional{ ?lexicalSense ddf:hasFrequency/skos:prefLabel ?frequency }.
   optional{ ?lexicalSense ddf:hasConnotation/skos:prefLabel ?connotation }.
   optional{ ?lexicalSense ddf:hasSociolect/skos:prefLabel ?sociolect }.
   optional{ ?lexicalSense ddf:hasGrammaticalConstraint/skos:prefLabel ?grammaticalConstraint  }.
   optional{ ?lexicalSense ddf:hasTextualGenre/skos:prefLabel ?textualGenre }.
   optional{ ?lexicalSense ddf:hasGlossary/skos:prefLabel ?glossary }.

   optional{ ?lexicalSense ddf:lexicalSenseHasSemanticRelationWith/ddf:hasSemanticRelationType/skos:prefLabel ?semanticRelation }.
   optional{ ?lexicalSense lexicog:usageExample ?exemple }.
   optional{ ?exemple rdf:value ?exempleValue }.
   OPTIONAL{ ?exemple dct:bibliographicalCitation ?exempleSource } .
}
 
LIMIT 200
```

# Autre version

```sparql 
# Tous les attributs ( que j'ai trouvé ) sur un lexicalSense donné (<http://data.dictionnairedesfrancophones.org/dict/wikt/sense/140ae3662fce0310dda46cb4e3c013f2>)
# équivalent à la requete /form/eau/sense/wkt%3Asense%2F140ae3662fce0310dda46cb4e3c013f2
  

PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX : <http://www.geonames.org/ontology#>

SELECT  distinct *
WHERE {    
   ?lexicalEntry ontolex:canonicalForm ?form .
   ?form ontolex:writtenRep ?writtenRep .
   ?lexicalEntry ontolex:sense ?lexicalSense . 
   ?lexicalSense skos:definition ?definition .
   FILTER( ?writtenRep = "eau"@fr )
   FILTER (?lexicalSense = <http://data.dictionnairedesfrancophones.org/dict/wikt/sense/140ae3662fce0310dda46cb4e3c013f2> ) 
   optional{ ?lexicalEntry ddf:hasPartOfSpeech/skos:prefLabel ?pos }.
   optional{ ?lexicalEntry ddf:multiWordType/skos:prefLabel ?multi } .
   optional{ { ?lexicalEntry ddf:hasGender/skos:prefLabel ?gender} UNION
           { ?lexicalEntry ddf:inflectionHasGender/skos:prefLabel ?gender}}.
   optional{ { ?lexicalEntry ddf:hasNumber/skos:prefLabel ?number } UNION
             { ?lexicalEntry ddf:inflectionHasNumber/skos:prefLabel ?number }} .
   optional{ ?lexicalEntry ddf:hasTransitivity/skos:prefLabel ?transitivity } .
   optional{ ?lexicalEntry lexinfo:termElement/skos:prefLabel ?term }.
   optional{ ?lexicalSense ddf:hasDomain/skos:prefLabel ?domain } .
   optional{ ?lexicalSense ddf:hasRegister/skos:prefLabel ?register } .
   optional{ ?lexicalSense ddf:hasSemanticProperty/skos:prefLabel ?semanticProperty }.
   optional{ ?lexicalSense ddf:hasTemporality/skos:prefLabel ?temporality }.
   optional{ ?lexicalSense ddf:hasFrequency/skos:prefLabel ?frequency }.
   optional{ ?lexicalSense ddf:hasConnotation/skos:prefLabel ?connotation }.
   optional{ ?lexicalSense ddf:hasSociolect/skos:prefLabel ?sociolect }.
   optional{ ?lexicalSense ddf:hasGrammaticalConstraint/skos:prefLabel ?grammaticalConstraint  }.
   optional{ ?lexicalSense ddf:hasTextualGenre/skos:prefLabel ?textualGenre }.
   optional{ ?lexicalSense ddf:hasGlossary/skos:prefLabel ?glossary }.

   optional{ ?lexicalSense ddf:lexicalSenseHasSemanticRelationWith/ddf:hasSemanticRelationType/skos:prefLabel ?semanticRelation }.
   optional{ ?lexicalSense lexicog:usageExample ?exemple }.
   optional{ ?exemple rdf:value ?exempleValue }.
   OPTIONAL{ ?exemple dct:bibliographicalCitation ?exempleSource } .
}
 
LIMIT 200
```


