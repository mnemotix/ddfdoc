# Entrées d’un glossaire avec relations et mots liés.

``` sparql

#Entrées d’un glossaire avec relations et mots liés.
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?word ?definition ?relation ?otherword
WHERE {
 ?entry a lexicog:Entry .
 ?entry lexicog:describes ?lentry .
 ?lentry ontolex:canonicalForm/ontolex:writtenRep ?word .
 ?lentry ontolex:sense ?sense .
 ?sense skos:definition ?definition .
 ?sense ddf:hasGlossary <http://data.dictionnairedesfrancophones.org/authority/glossary/chats> .
 ?lentry ddf:lexicalEntryHasSemanticRelationWith ?lien .
    ?lien ddf:hasSemanticRelationType/skos:prefLabel ?relation.
 ?lien ddf:semanticRelationOfEntry/lexicog:describes ?otherentry.
 ?otherentry ontolex:canonicalForm/ontolex:writtenRep ?otherword .
}
ORDER BY ?word
LIMIT 1000
```
