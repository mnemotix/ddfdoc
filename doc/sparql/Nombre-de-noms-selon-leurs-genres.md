# Nombre de définitions de noms selon leurs genres.

```sparql

#Nombre de définitions de noms selon leurs genres.
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT  ?genre (COUNT(DISTINCT ?def) AS ?compte)
 WHERE {
 ?entry a lexicog:Entry .
 ?entry lexicog:describes ?lentry .
 ?lentry ontolex:sense ?sense .
 ?sense skos:definition ?def .
 ?lentry ddf:hasPartOfSpeech <http://www.lexinfo.net/ontology/2.0/lexinfo#noun>.
 ?lentry ddf:hasGender ?gender .
    ?gender skos:prefLabel ?genre .
}
GROUP BY  ?gender ?genre
ORDER BY DESC (?compte)
