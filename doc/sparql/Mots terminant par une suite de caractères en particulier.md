# Entrées dont la forme termine par une suite de caractère en particulier.

```sparql

#Entrées dont la forme termine par une suite de caractère en particulier.
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT DISTINCT ?mot ?classe ?genre ?definition ?aire (GROUP_CONCAT(DISTINCT ?information ; separator=", ") as ?usage)
WHERE {
 ?entree a lexicog:Entry .
 ?entree lexicog:describes ?lentree .
 ?lentree ontolex:canonicalForm/ontolex:writtenRep ?mot .
  OPTIONAL{?lentree ddf:hasPartOfSpeech|ddf:multiWordType ?classeid .
        ?classeid skos:prefLabel ?classe .}
 ?lentree ontolex:sense ?sens .
 ?sens skos:definition ?definition .
  OPTIONAL{ ?lentree ddf:hasGender/skos:prefLabel ?genre.}
  FILTER ( regex(?mot, "oune$") )
  OPTIONAL{ ?sens ddf:hasLocalisation/skos:prefLabel ?aire.	}
  OPTIONAL { ?sens ddf:hasDomain|ddf:hasRegister|ddf:hasSociolect|ddf:hasConnotation|ddf:hasFrequency|ddf:hasTemporality ?info.
  			?info skos:prefLabel ?information }
}
GROUP BY ?mot ?classe ?genre ?definition ?aire
ORDER BY ?mot
LIMIT 1000
```
