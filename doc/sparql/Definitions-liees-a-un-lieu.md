# Définitions d'un lieu (ici le Togo) avec les informations disponibles.

```sparql 
#Définitions d'un lieu (ici le Togo) avec les informations disponibles.
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://www.w3.org/2004/02/skos/core#>
PREFIX prov: <https://www.w3.org/ns/prov#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX lemon: <http://lemon-model.net/lemon#>
PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?mot ?classe ?def ?exemple ?source ?domaine ?registre ?diachronique ?frequence ?connotation ?prononciation
 WHERE {
 ?entry a lexicog:Entry .
 ?entry lexicog:describes ?lentry .
 ?lentry ontolex:canonicalForm/ontolex:writtenRep ?mot .
  OPTIONAL {?lentry ontolex:canonicalForm/ontolex:phoneticRep ?prononciation .}
  OPTIONAL{?lentry ddf:hasPartOfSpeech|ddf:multiWordType ?classeid .
        ?classeid skos:prefLabel ?classe .}
 ?lentry ontolex:sense ?sense .
 ?sense skos:definition ?def .
  OPTIONAL{?sense lexicog:usageExample/rdf:value ?exemple .}
  OPTIONAL{?sense ddf:hasRegister/skos:prefLabel ?registre .}
  OPTIONAL{?sense ddf:hasDomain/skos:prefLabel ?domaine .}
  OPTIONAL{?sense ddf:hasTemporality/skos:prefLabel ?diachronique .}
  OPTIONAL{?sense ddf:hasFrequency/skos:prefLabel ?frequence .}
  OPTIONAL{?sense ddf:hasConnotation/skos:prefLabel ?connotation .}
 ?sense ddf:hasLocalisation <https://www.geonames.org/2363686>.
 ?entry ddf:hasLexicographicResource/skos:prefLabel ?source .
}
ORDER BY (?mot)
```
